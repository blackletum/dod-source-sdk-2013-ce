<p align="center">
  <img src=".assets/dods.png" width="200" height="200">
</p>

<div align="center">

</div>

# What is this? 
Dod+ (temp name) is a source mod for dod:s built upon SDK 2013 CE. Mor details coming soon 


# What's Source SDK 2013 Community Edition?
Source 2013 CE is a clean fork of Valve's [Source SDK 2013 repo](https://github.com/valveSoftware/source-sdk-2013) 
with the goal of fixing up the SDK and to provide a clean bloat-free codebase that works out of the box to make developers' lives easier.

# Compile Requirements 
To be able to use Source 2013 CE you will need to download **Visual Studio 2022** and install:
* MSVC v143 - VS 2022 C++ x64/x86 build tools
* C++ MFC Library for latest v143 build tools (x86 and x64)
* Windows 11 SDK (10.0.22000.0)

As of July 2023, CE has been tested on Visual Studio 2022 with the latest versions of the requirements listed above. So if desired you can use that instead.

Credits for 2019 support: momentum mod, Brae, and Anthonypython
Credits for CI: TF2Vintage, Deathreus, Dio, Anthonypython
Credits for Source SDK 2013 CE: Nbc66
